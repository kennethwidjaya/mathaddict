//
//  SoundVM.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 17/05/22.
//

import Foundation
import AVFoundation

class SoundVM: ObservableObject {
    
    var audioPlayer: AVAudioPlayer?
    var tapButtonSoundAP: AVAudioPlayer?
    var trueButtonSoundAP: AVAudioPlayer?
    var falseButtonSoundAP: AVAudioPlayer?
    
    func playSound(sound: String, type: String) {
        if let path = Bundle.main.path(forResource: sound, ofType: type) {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
                audioPlayer?.volume = 0.5
            } catch {
                print("ERROR")
            }
        }
    }
    
    func tapButtonSound(sound: String, type: String) {
        if let path = Bundle.main.path(forResource: sound, ofType: type) {
            do {
                tapButtonSoundAP = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            } catch {
                print("ERROR")
            }
        }
    }
    
    func trueButtonSound(sound: String, type: String) {
        if let path = Bundle.main.path(forResource: sound, ofType: type) {
            do {
                trueButtonSoundAP = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            } catch {
                print("ERROR")
            }
        }
    }
    
    func falseButtonSound(sound: String, type: String) {
        if let path = Bundle.main.path(forResource: sound, ofType: type) {
            do {
                falseButtonSoundAP = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            } catch {
                print("ERROR")
            }
        }
    }
}
