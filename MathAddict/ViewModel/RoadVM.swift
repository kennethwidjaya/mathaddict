//
//  RoadVM.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 09/05/22.
//

import Foundation
import SwiftUI

class RoadVM: ObservableObject {
    
    @Published var pathWH = CGPoint(x: 0, y: 0)
    @Published var roadSize = CGPoint(x: 0, y: 0)
    @Published var ballSize = CGPoint(x: 0, y: 0)
    @Published var locationBall = CGPoint(x: 0, y: 0)
    
    @Published var timerValue = Double(0)
    @Published var gameOver = false
    
    func ballMove(score : Int) {
        if locationBall.y < (roadSize.y - (ballSize.y / 2)) {
            withAnimation {
                locationBall.y += 1
            }
            for i in 1...5 {
                if timerValue > Double(5 * i) {
                    withAnimation {
                        locationBall.y += CGFloat(0.5 * Double(i))
                    }
                }
            }
            if locationBall.y < 0 - roadSize.y {
                locationBall.y = (0 - roadSize.y + (ballSize.y / 2))
            }
        } else {
            if LocalStorage.myHighScore < score {
                LocalStorage.myHighScore = score
            }
            gameOver = true
        }
    }
    
    func ballMoveTrueAnswer() {
        withAnimation {
            locationBall.y -= 75
        }
    }
    
    func ballMoveFalseAnswer() {
        withAnimation {
            locationBall.y += 35
        }
    }
}
