//
//  GameVM.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 27/04/22.
//

import Foundation

class GameVM: ObservableObject {
    
    @Published var correctAnswer = 0
    @Published var choiceAnswer : [Int] = [0, 1, 2, 3]
    var choiceCheckIfSameAnswer : [Int] = [0, 1, 2, 3]
    @Published var firstNumber = 0
    @Published var secondNumber = 0
    @Published var difficulty = 10
    @Published var score = 0
    @Published var trueAnswer = 0
    @Published var falseAnswer = 0
    
    func answerIsCorrect(answer: Int) {
        if answer == correctAnswer {
            self.score += 1
            self.trueAnswer += 1
        } else {
            if score > 0 {
                self.score -= 1
            }
            self.falseAnswer += 1
        }
    }
    
    func generateAnswers() {
        firstNumber = Int.random(in: 0...(difficulty/2))
        secondNumber = Int.random(in: 0...(difficulty/2))
        var answerList = [Int]()
        
        correctAnswer = firstNumber + secondNumber
        
        for _ in 0...2 {
            answerList.append(Int.random(in: 0...difficulty))
        }
        
        answerList.append(correctAnswer)
        
        choiceCheckIfSameAnswer = answerList.shuffled()
        
        checkIfAnswersSame()
    }
    
    func checkIfAnswersSame() {
        if choiceCheckIfSameAnswer[3] == choiceCheckIfSameAnswer[0] {
            generateAnswers()
        } else if choiceCheckIfSameAnswer[3] == choiceCheckIfSameAnswer[1] {
            generateAnswers()
        } else if choiceCheckIfSameAnswer[3] == choiceCheckIfSameAnswer[2] {
            generateAnswers()
        } else if choiceCheckIfSameAnswer[2] == choiceCheckIfSameAnswer[0] {
            generateAnswers()
        } else if choiceCheckIfSameAnswer[2] == choiceCheckIfSameAnswer[1] {
            generateAnswers()
        } else if choiceCheckIfSameAnswer[1] == choiceCheckIfSameAnswer[0] {
            generateAnswers()
        } else {
            choiceAnswer = choiceCheckIfSameAnswer
        }
    }
}
