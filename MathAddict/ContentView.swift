//
//  ContentView.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 27/04/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
//        DemoSceneKit()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
