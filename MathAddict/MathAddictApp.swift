//
//  MathAddictApp.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 27/04/22.
//

import SwiftUI

@main
struct MathAddictApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
