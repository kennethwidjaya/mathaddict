//
//  LocalStorage.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 11/05/22.
//

import Foundation

class LocalStorage {
    
    private static var myStorageHighScore: String = "myStorageHighScore"
    
    public static var myHighScore: Int {
        set {
            UserDefaults.standard.set(newValue, forKey: myStorageHighScore)
        }
        get {
            return UserDefaults.standard.integer(forKey: myStorageHighScore)
        }
    }
    
    public static func removeValue() {
        UserDefaults.standard.removeObject(forKey: myStorageHighScore)
    }
}
