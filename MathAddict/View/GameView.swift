//
//  GameView.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 27/04/22.
//

import SwiftUI

struct GameView: View {
    
    let buttonColor = [Color.yellow, .blue, .red, .mint]
    
    @ObservedObject var gameVM = GameVM()
    @ObservedObject var roadVM = RoadVM()
    @ObservedObject var soundVM: SoundVM
    
    @Binding var tapSound: Bool
    
    @State private var screenTap = false
    @State private var backgroundColor = Color.white
    @Binding var mulaiBermain: Bool
    
    let timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        ZStack {
            ZStack {
                RadialGradient(colors: [.green, .darkGreen], center: .center, startRadius: 1, endRadius: 500)
                
                LinearGradient(colors: [.lightBrown, .darkBrown], startPoint: .topLeading, endPoint: .trailing)
                    .mask(RoundedRectangle(cornerRadius: UIScreen.main.bounds.height/15)
                        .stroke(lineWidth: UI_HEIGHT/40))
                
                LinearGradient(colors: [.lightBrown, .darkBrown], startPoint: .topLeading, endPoint: .trailing)
                    .mask(Rectangle()
                        .stroke(lineWidth: UI_HEIGHT/50))
            }
            .ignoresSafeArea()
            
            VStack {
                ZStack {
                    GeometryReader { geo in
                        let geoRoadWH = CGPoint(x: geo.size.width, y: geo.size.height)
                        Rectangle()
                            .foregroundColor(backgroundColor)
                            .frame(width: geoRoadWH.x / 2, height: geoRoadWH.y * 2)
                            .position(x: geoRoadWH.x / 2)
                            .offset(y: -15)
                            .rotation3DEffect(.degrees(45), axis: (x: 1, y: 0, z: 0))
                            .onAppear {
                                roadVM.roadSize = CGPoint(x: geoRoadWH.x, y: geoRoadWH.y)
                            }
                        
                    }
                    
//                    ForEach(0 ..< 9) { num in
//                        GeometryReader { geo in
//                            let geoPathWH = CGPoint(x: geo.size.width, y: geo.size.height)
//                            Rectangle()
//                                .foregroundColor(.black)
//                                .frame(width: geoPathWH.x / 2, height:  geoPathWH.y / 20)
//                                .position(x: geoPathWH.x / 2, y: geoPathWH.y / 1.1 - CGFloat(num * 90))
//                                .offset(y: -15)
//                                .rotation3DEffect(.degrees(45), axis: (x: 1, y: 0, z: 0))
//                                .onAppear {
//                                    roadVM.pathWH = geoPathWH
//                                    print(roadVM.pathWH)
//                                }
//                        }
//                        .onReceive(timer) { _ in
//                            
//                        }
//                    }
                    
                    GeometryReader { geo in
                        let geoBallWH = CGPoint(x: geo.size.width, y: geo.size.height)
                        Circle()
                            .foregroundColor(.cyan)
                            .frame(width: roadVM.ballSize.x, height: roadVM.ballSize.y)
                            .position(roadVM.locationBall)
                            .offset(y: -15)
                            .rotation3DEffect(.degrees(45), axis: (x: 5, y: 0, z: 0))
                            .shadow(color: .black, radius: 3, x: 0, y: 3)
                            .onReceive(timer, perform: { _ in
                                roadVM.ballMove(score: gameVM.score)
                                roadVM.timerValue += 0.1
                            })
                            .onAppear {
                                roadVM.locationBall = CGPoint(x: geoBallWH.x / 2, y: geoBallWH.y / 1.15)
                                roadVM.ballSize = CGPoint(x: geoBallWH.x / 10, y: geoBallWH.y / 10)
                            }
                            .onChange(of: gameVM.trueAnswer) { _ in
                                roadVM.ballMoveTrueAnswer()
                                if tapSound == true {
                                    soundVM.trueButtonSoundAP?.currentTime = 0
                                    soundVM.trueButtonSoundAP?.play()
                                }
                                backgroundColor = Color.green
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    backgroundColor = Color.white
                                }
                            }
                            .onChange(of: gameVM.falseAnswer) { _ in
                                roadVM.ballMoveFalseAnswer()
                                if tapSound == true {
                                    soundVM.falseButtonSoundAP?.currentTime = 0
                                    soundVM.falseButtonSoundAP?.play()
                                }
                                backgroundColor = Color.red
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    backgroundColor = Color.white
                                }
                            }
                    }
                    
                    VStack {
                        HStack(alignment: .center) {
                            HStack {
                                Button {
                                    mulaiBermain = false
                                    if tapSound == true {
                                        soundVM.tapButtonSoundAP?.currentTime = 0
                                        soundVM.tapButtonSoundAP?.play()
                                    }
                                } label: {
                                    Image(systemName: "chevron.left")
                                        .font(.largeTitle)
                                        .foregroundColor(.black)
                                        .frame(width: 45, height: 45)
                                        .overlay(Circle()
                                            .stroke(lineWidth: 1)
                                            .foregroundColor(.black))
                                }
                            }
                            
                            Spacer()
                            
                            VStack {
                                Text("Score")
                                    .font(.title)
                                
                                Text("\(gameVM.score)")
                                    .font(.largeTitle)
                                    .fontWeight(.bold)
                            }
                            .foregroundColor(.white)
                        }
                        Spacer()
                    }
                    .padding(.horizontal)
                }
                
                Spacer()
                
                VStack(spacing: 10) {
                    Text("\(gameVM.firstNumber)  +  \(gameVM.secondNumber)")
                        .fontWeight(.bold)
                        .font(.system(size: 50))
                        .foregroundColor(.yellow)
                        .frame(width: UI_WIDTH / 1.5)
                        .overlay(RoundedRectangle(cornerRadius: 2)
                            .stroke(lineWidth: 8)
                            .foregroundColor(Color.darkBrown))
                        .padding(.top)
                    
                    VStack {
                        HStack {
                            Spacer()
                            ForEach(0 ..< 2) { num in
                                Button {
                                    gameVM.answerIsCorrect(answer: gameVM.choiceAnswer[num])
                                    gameVM.generateAnswers()
                                } label: {
                                    AnswerButton(number: gameVM.choiceAnswer[num], backgroundColor: buttonColor[num])
                                }
                            }
                            Spacer()
                        }
                        
                        HStack {
                            ForEach(2 ..< 4) { num in
                                Button {
                                    gameVM.answerIsCorrect(answer: gameVM.choiceAnswer[num])
                                    gameVM.generateAnswers()
                                } label: {
                                    AnswerButton(number: gameVM.choiceAnswer[num], backgroundColor: buttonColor[num])
                                }
                            }
                        }                        
                    }
                }
            }
            .padding(.horizontal)
            
            if roadVM.gameOver == true {
                ZStack {
                    ZStack {
                        RadialGradient(colors: [.green, .darkGreen], center: .center, startRadius: 1, endRadius: 500)
                        
                        LinearGradient(colors: [.lightBrown, .darkBrown], startPoint: .topLeading, endPoint: .trailing)
                            .mask(RoundedRectangle(cornerRadius: UIScreen.main.bounds.height/15)
                                .stroke(lineWidth: UI_HEIGHT/40))
                        
                        LinearGradient(colors: [.lightBrown, .darkBrown], startPoint: .topLeading, endPoint: .trailing)
                            .mask(Rectangle()
                                .stroke(lineWidth: UI_HEIGHT/50))
                    }
                    .ignoresSafeArea()
                    
                    VStack {
                        Text("MATH ADDICT")
                            .font(.largeTitle)
                            .fontWeight(.bold)
                            .foregroundColor(.lightBrown)
                            .padding(10)
                            .padding(.horizontal)
                            .overlay(RoundedRectangle(cornerRadius: 10)
                                .stroke(lineWidth: 5)
                                .foregroundColor(.darkBrown))
                            .padding(.top)
                        
                        Spacer()
                        
                        VStack {
                            Text("Kamu Kalah")
                                .fontWeight(.bold)
                            Text("Score")
                                .fontWeight(.bold)
                            Text("\(gameVM.score)")
                                .font(.system(size: 75))
                        }
                        .foregroundColor(.white)
                        .font(.largeTitle)
                        .padding(.bottom)
                        
                        VStack {
                            Text("Score Tertinggi")
                                .fontWeight(.bold)
                            Text("\(LocalStorage.myHighScore)")
                                .font(.system(size: 75))
                        }
                        .foregroundColor(.white)
                        .font(.largeTitle)
                        
                        Spacer()
                        
                        Button {
                            roadVM.gameOver = false
                            mulaiBermain = false
                            if tapSound == true {
                                soundVM.tapButtonSoundAP?.currentTime = 0
                                soundVM.tapButtonSoundAP?.play()
                            }
                        } label: {
                            Text("Kembali Ke Home")
                                .font(.title3)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                                .padding(10)
                                .overlay(RoundedRectangle(cornerRadius: 5)
                                    .stroke(lineWidth: 3)
                                    .foregroundColor(.white).shadow(color: .black, radius: 2, x: 0, y: 2))
                        }
                        .padding(.top)
                    }
                }
            }
        }
        .onAppear{
            gameVM.generateAnswers()
        }
    }
}

//struct GameView_Previews: PreviewProvider {
//    
//    @State static var value = true
//    
//    static var previews: some View {
//        GameView(mulaiBermain: $value)
//    }
//}
