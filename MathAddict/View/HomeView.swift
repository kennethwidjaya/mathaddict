//
//  HomeView.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 27/04/22.
//

import SwiftUI

struct HomeView: View {
    
    @ObservedObject var soundVM = SoundVM()
    
    @State var bgSound = true
    @State var tapSound = true
    @State var mulaiBermain = false
    @State var caraBermain = false
    
    var body: some View {
        ZStack {
            ZStack {
                RadialGradient(colors: [.green, .darkGreen], center: .center, startRadius: 1, endRadius: 500)
                
                LinearGradient(colors: [.lightBrown, .darkBrown], startPoint: .topLeading, endPoint: .trailing)
                    .mask(RoundedRectangle(cornerRadius: UIScreen.main.bounds.height/15)
                        .stroke(lineWidth: UI_HEIGHT/40))
                
                LinearGradient(colors: [.lightBrown, .darkBrown], startPoint: .topLeading, endPoint: .trailing)
                    .mask(Rectangle()
                        .stroke(lineWidth: UI_HEIGHT/50))
            }
            .ignoresSafeArea()
            
            VStack {
                HStack {
                    Spacer()
                    
                    Text("MATH ADDICT")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .foregroundColor(.lightBrown)
                        .padding(10)
                        .padding(.horizontal)
                        .overlay(RoundedRectangle(cornerRadius: 10)
                            .stroke(lineWidth: 5)
                            .foregroundColor(.darkBrown))
                        .padding(.top)
                    
                    Spacer()
                }
                
                Spacer()
                
                Button {
                    mulaiBermain = true
                    if tapSound == true {
                        soundVM.tapButtonSoundAP?.currentTime = 0
                        soundVM.tapButtonSoundAP?.play()
                    }
                } label: {
                    Text("Mulai Bermain")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .padding(10)
                        .padding(.horizontal)
                        .overlay(RoundedRectangle(cornerRadius: 10)
                            .stroke(lineWidth: 5)
                            .foregroundColor(.white)
                            .shadow(color: .black, radius: 3, x: 0, y: 3))
                }
                
                Spacer()
                
                HStack(alignment: .bottom) {
                    VStack {
                        VStack {
                            Text("BMG")
                                .font(.headline)
                                .foregroundColor(.white)
                            
                            Image(systemName: bgSound ? "speaker" : "speaker.slash")
                                .font(.system(size: 20))
                                .foregroundColor(.white)
                                .padding(10)
                                .overlay(RoundedRectangle(cornerRadius: 5)
                                    .stroke(lineWidth: 3)
                                    .foregroundColor(.white).shadow(color: .black, radius: 2, x: 0, y: 2))
                        }
                        
                        VStack {
                            Text("Tap")
                                .font(.headline)
                                .foregroundColor(.white)
                            
                            Image(systemName: bgSound ? "speaker" : "speaker.slash")
                                .font(.system(size: 20))                                .foregroundColor(.white)
                                .padding(10)
                                .overlay(RoundedRectangle(cornerRadius: 5)
                                    .stroke(lineWidth: 3)
                                    .foregroundColor(.white).shadow(color: .black, radius: 2, x: 0, y: 2))
                        }
                        
                    }
                    .opacity(0)
                    
                    Spacer()
                    
                    Button {
                        caraBermain = true
                        if tapSound == true {
                            soundVM.tapButtonSoundAP?.currentTime = 0
                            soundVM.tapButtonSoundAP?.play()
                        }
                    } label: {
                        Text("Cara Bermain")
                            .font(.title3)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(10)
                            .overlay(RoundedRectangle(cornerRadius: 5)
                                .stroke(lineWidth: 3)
                                .foregroundColor(.white).shadow(color: .black, radius: 2, x: 0, y: 2))
                        
                    }
                    
                    Spacer()
                    
                    VStack {
                        VStack {
                            Text("BGM")
                                .font(.headline)
                                .foregroundColor(.white)
                        Button {
                            bgSound.toggle()
                            if bgSound == true {
                                soundVM.audioPlayer?.play()
                            } else {
                                soundVM.audioPlayer?.pause()
                            }
                        } label: {
                                Image(systemName: bgSound ? "speaker" : "speaker.slash")
                                    .font(.system(size: 20))
                                    .foregroundColor(.white)
                                    .padding(10)
                                    .frame(height: 40)
                                    .overlay(RoundedRectangle(cornerRadius: 5)
                                        .stroke(lineWidth: 3)
                                        .foregroundColor(.white).shadow(color: .black, radius: 2, x: 0, y: 2))
                            }
                        }
                        VStack {
                            Text("Tap")
                                .font(.headline)
                                .foregroundColor(.white)
                            
                        Button {
                            tapSound.toggle()
                            if tapSound == true {
                                soundVM.tapButtonSoundAP?.currentTime = 0
                                soundVM.tapButtonSoundAP?.play()
                            }
                        } label: {
                                Image(systemName: tapSound ? "speaker" : "speaker.slash")
                                    .font(.system(size: 20))
                                    .foregroundColor(.white)
                                    .padding(10)
                                    .frame(height: 40)
                                    .overlay(RoundedRectangle(cornerRadius: 5)
                                        .stroke(lineWidth: 3)
                                        .foregroundColor(.white).shadow(color: .black, radius: 2, x: 0, y: 2))
                            }
                        }
                    }
                }
                .padding(.bottom)
                .padding(.horizontal)
            }
            .padding(.horizontal)
            
            if mulaiBermain == true {
                GameView(soundVM: soundVM, tapSound: $tapSound, mulaiBermain: $mulaiBermain)
            }
            
            if caraBermain == true {
                CaraBermainView(soundVM: soundVM, tapSound: $tapSound, caraBermain: $caraBermain)
            }
        }
        .onAppear {
            soundVM.playSound(sound: "bgSound", type: "mp3")
            soundVM.audioPlayer?.numberOfLoops = 100
            soundVM.tapButtonSound(sound: "tapSound", type: "wav")
            soundVM.trueButtonSound(sound: "correctSound", type: "wav")
            soundVM.falseButtonSound(sound: "incorrectSound", type: "wav")
            if bgSound == true {
                soundVM.audioPlayer?.play()
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

extension Color {
    static let darkGreen = Color(red: 0.142, green: 0.361, blue: 0.138)
    static let lightBrown = Color(red: 0.897, green: 0.677, blue: 0.177)
    static let darkBrown = Color(red: 0.558, green: 0.252, blue: 0.117)
}
