//
//  CaraBermainView.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 10/05/22.
//

import SwiftUI

struct CaraBermainView: View {
    
    @ObservedObject var soundVM: SoundVM
    
    @Binding var tapSound: Bool
    @Binding var caraBermain: Bool
    
    var body: some View {
        ZStack {
            Color.black
                .opacity(0.8)
                .ignoresSafeArea()
            
            VStack {
                Text("Cara Bermain")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundColor(.lightBrown)
                    .padding(10)
                    .padding(.horizontal)
                    .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(lineWidth: 5)
                        .foregroundColor(.white))
                    .padding(.top)
                    .padding(.bottom)
                
                VStack(alignment: .leading) {
                    HStack {
                        Text("- Pilih jawaban yang benar untuk bertahan di permainan ini")
                        
                        Spacer()
                    }
                    
                    Text("- Selamat bermain Good Luck and Have Fun :)")
                }
                .foregroundColor(.white)
                
                Spacer()
            }
            .padding(.horizontal)
        }
        .onTapGesture {
            caraBermain = false
            if tapSound == true {
                soundVM.tapButtonSoundAP?.currentTime = 0
                soundVM.tapButtonSoundAP?.play()
            }
        }
    }
}

//struct CaraBermainView_Previews: PreviewProvider {
//    
//    @State static var caraBermain = false
//    
//    static var previews: some View {
//        CaraBermainView(caraBermain: $caraBermain)
//    }
//}
