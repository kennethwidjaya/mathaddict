//
//  AnswerButton.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 27/04/22.
//

import SwiftUI

struct AnswerButton: View {
    
    var number: Int
    var backgroundColor: Color
    
    var body: some View {
        Text("\(number)")
            .font(.system(size: 50))
            .fontWeight(.bold)
            .frame(width: 100, height: 100)
            .foregroundColor(.black)
            .background(backgroundColor)
            .clipShape(Rectangle())
            .shadow(color: .black, radius: 3, x: 0, y: 3)
            .padding()
    }
}

struct AnswerButton_Previews: PreviewProvider {
    static var previews: some View {
        AnswerButton(number: 0, backgroundColor: .yellow)
    }
}
