//
//  Constant.swift
//  MathAddict
//
//  Created by Kenneth Widjaya on 27/04/22.
//

import Foundation
import UIKit

let UI_WIDTH = UIScreen.main.bounds.width
let UI_HEIGHT = UIScreen.main.bounds.height
